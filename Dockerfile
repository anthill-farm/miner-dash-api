FROM node:lts-alpine3.17

WORKDIR /dash-api

COPY package*.json .
RUN npm install
COPY . .

EXPOSE 8000

ENTRYPOINT [ "npm", "run", "watch", "--", "--miner" ]
CMD [ "btm-x19" ]
