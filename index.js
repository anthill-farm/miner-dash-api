const Handlebars = require("handlebars");
const swaggerUiWatcher = require("./swagger-watcher/index.js");

const modelGroups = {
    "antminer": ["btm-x19"],
//    "antminer": ["btm-x17", "btm-x19"],
//    "innosilicon": ["inno-t2t"],
};

// npm run watch -- --miner inno-t2t

const preprocessorData = {
    minerModel: "btm-x19",
    minerMaker: "antminer",
};

Handlebars.registerHelper('modelSnakeCase', function () {
    return this.minerModel.split("-").join("_");
})

Handlebars.registerHelper('modelUpperCase', function () {
    return this.minerModel.toUpperCase();
})


// https://ru.code-maven.com/handlebars-conditionals
Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if (a == b) {
        return opts.fn(this);
    } else {
        return opts.inverse(this);
    }
});

function setMinerModel(minerModel) {
    minerModel = minerModel || "btm-x19";

    for (const [maker, models] of Object.entries(modelGroups)) {
        if (models.includes(minerModel)) {
            preprocessorData.minerModel = minerModel;
            preprocessorData.minerMaker = maker;
            return;
        }
    }

    console.error(`Not acceptable miner model "${minerModel}"`);
    process.exit(1);
}

function preprocessor (inputYamlString) {
    const template = Handlebars.compile(inputYamlString);
    return template(preprocessorData);
}

swaggerUiWatcher.startProgram(setMinerModel, preprocessor);
