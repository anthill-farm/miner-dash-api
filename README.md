### MinerDash API
___

#### Install dependencies
    npm install

#### View in browser (default s19)
    npm run watch
    npm run watch -- --miner btm-x19

#### Make *bundle.json* (default s19)
    npm run bundle
    npm run bundle -- --miner btm-x19

#### Build and run with Docker
```
# Build docker image
docker build -t miner-dash-api .

# Run container with api site
docker run -p 8000:8000 miner-dash-api

# You can specify the miner seriles as a parameter
docker run -p 8000:8000 miner-dash-api btm-x19
```
---
`swagger-watcher` folder is a fork of [swagger-ui-watcher](https://www.npmjs.com/package/swagger-ui-watcher)
with adding [handlebars](https://handlebarsjs.com/) as preprocessor.
To build miner dependent documentation using `{{ minerModel }}` templating
